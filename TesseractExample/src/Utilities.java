import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Utilities {

	// Convert String to Tokens
	public static List<String> LinetoStr(String inp_line) {
		List<String> inp_line_tokens = new ArrayList<>();
		StringTokenizer inp_line_stkn = new StringTokenizer(inp_line, " ");
		while (inp_line_stkn.hasMoreElements()) {
			inp_line_tokens.add(inp_line_stkn.nextToken());
		}
		return inp_line_tokens;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
