import java.util.Hashtable;
import java.util.List;
import static java.util.Arrays.asList;
import java.util.Enumeration;

public class ExtractTestResults {

	// Test related unique words
	private static final List<String> triiodothyronine_t3 = asList("triiodothyronine", "t3");
	private static final List<String> thyroxine_t4 = asList("thyroxine", "t4");
	private static final List<String> stimulating_hormone_tsh = asList("stimulating hormone", "tsh");
	
	
	// hash-table of test ids 
	static Hashtable<String, List<String>> hash_test_value =
			new Hashtable<String, List<String>>();
	
	static {
		hash_test_value.put("total_triiodothyronine", triiodothyronine_t3);
		hash_test_value.put("total_thyroxine", thyroxine_t4);
		hash_test_value.put("thyroid_stimulating_hormone", stimulating_hormone_tsh);
	}
	
	public static boolean testLineforResults(String inp_str) {
		List<String> line_str = Utilities.LinetoStr(inp_str);
		String test_type = "null";
		
	    for(int i =0; i < items.length; i++) {
	        if(inputStr.contains(items[i])) {
	            return true;
	        }
	    }
	    return false;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
