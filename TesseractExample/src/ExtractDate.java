import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer; 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import static java.util.Arrays.asList;

public class ExtractDate {	

	private String line_value;
	
	//Constructor
	public ExtractDate(String line_result) {
		line_value = line_result;
	}

	//Date formats in a list
	private static final List<String> dateFormats = asList("dd/mm/yyyy", "dd.MMM.yyyy", "dd MMM yyyy");
	
	
	// Date format validation
	public static boolean ValidateDateString(String datetocheck) {
		boolean str_test = false;
		for (String pattern : ExtractDate.dateFormats) {
			try {
				new SimpleDateFormat(pattern).parse(datetocheck);
				str_test = true;
			}
			catch (ParseException e) {}
		}
		return str_test;
	}

	//Check if line has date format
	public static boolean hasDate(String inp_str) {
		boolean date_check = false;
		List<String> line_str = Utilities.LinetoStr(inp_str);
		for (String item: line_str) {
			date_check = ExtractDate.ValidateDateString(item);
			if (date_check == true) {
					break;
			}
		}
		return date_check;
	}
	
	// Check if string has date information.
	public static boolean DateLine(String line_value) {
		if (line_value.indexOf("sample collected") != -1) {
			return true;
		} else if (line_value.indexOf("date") != -1) {
			return true;
		} else if (ExtractDate.hasDate(line_value) == true) {
			return true;
		} else {
			return false;
		}
	}
	
	// Extract date into required format
	
	//1. Convert String to date
	public static Date StringtoDate (String inp_str) {
		Date strdate = new Date();
		for (String pattern : ExtractDate.dateFormats) {
			try {
				strdate = new SimpleDateFormat(pattern).parse(inp_str);
				break;
			}
			catch (ParseException e) {
				strdate = null;
			}
		}
		return strdate;
	}
	
	//2. Process line to extract date
	public static Date DatefrmLine (String line_value) {
		Date date_value = new Date();
		date_value = null;
		List<String> line_str = Utilities.LinetoStr(line_value);
		for (String item : line_str) {
			date_value = ExtractDate.StringtoDate(item);
			if (date_value != null) {
				break;
			}
		}
		if (date_value == null) {	//for date format with space. 
			String seperator = " ";
			for (int i=0; i+2 < line_str.size(); i++ ) {
			String item = line_str.get(i) + seperator + line_str.get(i+1) + seperator + line_str.get(i+2);
			//System.out.println(item);
			date_value = ExtractDate.StringtoDate(item);
			if (date_value != null) {
				break;
			}
			}
		}
		return date_value;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String line = "sample collected on : 11 mar 2015";// is name";
		boolean date_test = ExtractDate.DateLine(line);
		System.out.println(date_test);
		Date date_check = ExtractDate.DatefrmLine(line);
		System.out.println(date_check);
		/*
		List<String> line_str = ExtractDate.LinetoStr(line);
		for (String tmp: line_str) {
			System.out.println ( tmp );
			boolean date_check = ExtractDate.ValidateDateString(tmp);
			System.out.println ( date_check );
		}
		*/

	}

}
